import click
import os

import ukc_search as ukcs
import ukc_search.download_search_page
import ukc_search.extract_crags
import ukc_search.download_crags
import ukc_search.extract_routes
import ukc_search.filter_crags

@click.command()
@click.option('--location',
        default='Bristol',
        help='Centre of search.')
@click.option('--distance',
        default='50',
        help='Radius of search.')
@click.option('--route_type_filter',
        default = 'sport',
        help='Filter by route type sport etc.')
@click.option('--route_rating_filter',
        default=0,
        help='Filter by * rating.')
@click.option('--route_cnt_filter',
        default = 50,
        help='Filter by number of routes matching filters.')
@click.option('--min_grade',
        default = None,
        help='Filter to just routes this grade or harder')
@click.option('--max_grade',
        default = None,
        help='Filter to just routes this grade or easier')
def search(location, distance, route_type_filter,
        route_rating_filter, route_cnt_filter,
        min_grade, max_grade):
    """Return URLs of crags that match filters."""

    # Download html of pages of normal ukc search results
    search_url = 'https://www.ukclimbing.com/logbook/map/?g=0&loc={}&dist={}&km=1&q=&rock=0&dir=0&day=0&rain=0#main'.format(
            location, distance)
    search_pages = ukcs.download_search_page.download_search_pages(search_url)

    # Extract the ids of crags from those search results
    all_crags = ukcs.extract_crags.extract_all_crags(search_pages)

    # Select crags with enough routes (of any type, further filtering later)
    selected_crags = ukcs.download_crags.select_crags(
            all_crags, min_route_count = route_cnt_filter
            )
    selected_crags = ukcs.download_crags.download_crags(selected_crags)

    # Extract information about every route on these crags
    routes = ukcs.extract_routes.extract_all_crag_routes(selected_crags)

    # Filter crags and display urls
    df = ukcs.filter_crags.filter_crags(
            routes, route_type_filter, route_rating_filter, route_cnt_filter,
            min_grade, max_grade)
    ukcs.filter_crags.print_filtered_crags(df)


if __name__ == '__main__':
    search()
