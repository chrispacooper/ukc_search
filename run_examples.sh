set -e

echo "TEST bouldering"
python search.py --location Sheffield --distance 15 --route_type_filter bouldering --route_cnt_filter 50

echo "TEST bouldering filtered by grade"
python search.py --location Sheffield --distance 15 --route_type_filter bouldering --route_cnt_filter 50 --min_grade f7a --max_grade f7c

echo "TEST trad"
python search.py --location Pembroke --distance 50 --route_type_filter trad --route_cnt_filter 200

echo "TEST trad filtered by min grade"
python search.py --location Pembroke --distance 50 --route_type_filter trad --route_cnt_filter 200 --min_grade "HVS"

echo "TEST trad filtered by max grade"
python search.py --location Pembroke --distance 50 --route_type_filter trad --route_cnt_filter 200 --max_grade "HVS"

echo "TEST sport filtered by grade"
python search.py --location Bristol --distance 50 --route_type_filter sport --route_cnt_filter 100 --min_grade 6a --max_grade 7a

echo "TEST sport filtered by grade and rating"
python search.py --location Bristol --distance 50 --route_type_filter sport --route_cnt_filter 10 --min_grade 7a --max_grade 7c --route_rating_filter 3

echo "TEST trad (happens to include uninterpretable grade)"
python search.py --location Kendall --distance 50 --route_type_filter trad --route_cnt_filter 50 --min_grade HVS
