# Installation
Requires at least python 3.6
```
git clone https://gitlab.com/chrispacooper/ukc_search
cd ukc_search
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python setup.py develop
```

# Use

This will return a list of crags within 50km of Bristol with at least 100 sport routes.
```
python search.py --location Bristol --distance 50 --route_type_filter sport --route_cnt_filter 100
```

Here are a couple of other example searches:
```
python search.py --location Sheffield --distance 15 --route_type_filter bouldering --route_cnt_filter 50
python search.py --location Pembroke --distance 50 --route_type_filter trad --route_cnt_filter 200
```

See run_examples.sh for more.

# Developing

Please use flake8 and pydocstyle to enforce style.

Please ensure the tests pass and the examples run, like this:
```
py.test
bash run_examples.sh
```