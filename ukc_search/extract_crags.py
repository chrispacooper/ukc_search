from lxml import html
import re


def extract_crags(search_page):

    print("Extracting crag information")
    tree = html.fromstring(search_page['webpage'])

    # Extract the text containing the number of results
    x_selector = '//div[contains(@class, "panel panel-")]'
    x_selection = tree.xpath(x_selector)

    crags = []
    for x in x_selection:
        crag_id = extract_crag_id(x)
        has_routes, cnt_routes = extract_crag_cnt_routes(x)
        crags.append({
            "crag_id": crag_id,
            "has_routes": has_routes,
            "cnt_routes": cnt_routes,
            })

    return(crags)


def extract_crag_id(element):
    crag_id_raw = element.get('onclick')

    crag_id = re.findall('crag([0-9]+)', crag_id_raw)
    assert len(crag_id) == 1

    return(crag_id[0])


def extract_crag_cnt_routes(element):
    cnt_climbs_raw = element.xpath('div[2]/span[1]/text()')

    if len(cnt_climbs_raw) == 0:
        cnt_climbs_raw = ['']
        no_text_present = True

    assert len(cnt_climbs_raw) == 1

    cnt_routes = re.findall('^([0-9]+) climb', cnt_climbs_raw[0])

    single_mountain = re.search('[0-9]+ m', cnt_climbs_raw[0])
    unknown_routes = re.search(r'^\?\? climb', cnt_climbs_raw[0])

    if len(cnt_routes) == 1:
        has_routes = True
        cnt = cnt_routes[0]
    elif single_mountain or unknown_routes or no_text_present:
        has_routes = False
        cnt = ''
    else:
        raise Exception('Failed parsing text: {}'.format(cnt_climbs_raw))

    return(has_routes, cnt)


def extract_all_crags(search_pages):
    all_crags = []
    for search_page in search_pages:
        print("Extracting crags from: {}".format(search_page['url']))
        crags = extract_crags(search_page)
        print("{} crags extracted".format(len(crags)))
        all_crags = all_crags + crags

    print('{} crags available'.format(len(all_crags)))
    return(all_crags)
