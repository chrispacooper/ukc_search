from ukc_search import extract_routes


def test_route_type():

    sport_examples = [
            '         6c+ ***        ',
            ]
    for s in sport_examples:
        assert extract_routes.extract_route_type(s)[0] == "sport"

    bouldering_examples = [
            '              f7A+  *            ',
            '\r\n              f6C+              ',
            ]
    for b in bouldering_examples:
        assert extract_routes.extract_route_type(b)[0] == "bouldering"

    trad_examples = [
            '             E1 6a            ',
            '\r\n              E10 6b              ',
            '\r\n              HVS 6b              ',
            '\r\n              VS 4a              ',
            '\r\n              M 6b              ',
            '\r\n              HS 6b              ',
            '\r\n              D              ',
            ]
    for t in trad_examples:
        assert extract_routes.extract_route_type(t)[0] == "trad"

    unknown_examples = [
            '\r\n              V              ',
            ]
    for u in unknown_examples:
        assert extract_routes.extract_route_type(u)[0] == "unknown"
