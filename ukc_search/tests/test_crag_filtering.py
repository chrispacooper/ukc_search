import pandas as pd
from ukc_search.filter_crags import filter_crags


def test_crag_filtering():

    # Check one random example of filtering

    routes = pd.read_csv('ukc_search/tests/data/routes.csv')
    routes['route_rating'] = 0
    df = filter_crags(routes, 'bouldering', 0, 5)
    assert(set(df.crag_id) == {881, 8982})
