from ukc_search.grade_class import sport_grade, boulder_grade, trad_grade


def test_sport_grade():

    a = sport_grade("7a")
    b = sport_grade("8a")
    c = sport_grade("7c")

    # Self comparisons
    for g in (a, b, c):
        assert(g >= g)
        assert(g <= g)

    # Greater than
    assert(b >= c)
    assert(b >= a)
    assert(c >= a)

    # Less than
    assert(c <= b)
    assert(a <= b)
    assert(a <= c)

    # Grades that can't be compared
    ungradeable = sport_grade("sldkfjl")
    assert(not(ungradeable >= a))
    assert(not(ungradeable <= a))


def test_boulder_grade():

    a = boulder_grade("f5+")
    b = boulder_grade("V5 f6c *")
    c = boulder_grade("f7A")
    d = boulder_grade("V9")
    e = boulder_grade("f8A")

    # Self comparisons
    for g in (a, b, c, d, e):
        assert(g >= g)
        assert(g <= g)

    # Greater than
    assert(e >= d >= c >= b >= a)

    # Less than
    assert(a <= b <= c <= d <= e)

    # Grades that can't be compared
    ungradeable = boulder_grade("sldkfjl")
    assert(not(ungradeable >= a))


def test_trad_grade():

    a = trad_grade("HVS 5a")
    b = trad_grade("E1 5a")
    c = trad_grade("E5 6c/7a")

    for g in (a, b, c):
        assert(g >= g)

    assert(a <= b <= c)

    assert(c >= b >= a)

    # These exist on ukc but I don't know how hard they should be
    trad_grade("MS")
    trad_grade("HD")

    # Grades that can't be compared
    ungradeable = trad_grade("sldkfjl")
    assert(not(ungradeable >= a))
