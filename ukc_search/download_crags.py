import requests
import time
import random
import requests_cache

requests_cache.install_cache()


def select_crags(all_crags, min_route_count):

    print("Selecting crags")

    selected_crags = [
            c for c in all_crags if
            c['has_routes'] and int(c['cnt_routes']) >= min_route_count
            ]

    print('{} crags selected'.format(len(selected_crags)))

    return(selected_crags)


def download_crags(selected_crags):

    for crag in selected_crags:
        crag['url'], crag['webpage'] = download_html(crag)

    return(selected_crags)


def download_html(crag):
    url = 'https://www.ukclimbing.com/logbook/crag.php?id=' + crag['crag_id']

    print("Downloading {}".format(url))
    r = requests.get(url)
    print("From cache: {}".format(r.from_cache))
    if not r.from_cache:
        time.sleep(random.uniform(1, 5))

    return(url, r.text)
