from lxml import html
import re
import ukc_search.grade_class as ukc_class


def extract_crag_routes(crag):

    print("Extracting routes for crag {}".format(crag['url']))
    tree = html.fromstring(crag['webpage'])

    # Extract the text containing the number of results
    x_selector = '//tr[contains(@class, "climb")]'
    x_selection = tree.xpath(x_selector)

    routes = []
    for element in x_selection:
        grade_raw = element.xpath('td[5]')[0].text_content()

        route_type, grade = extract_route_type(grade_raw)

        routes.append({
            "grade": grade,
            "route_type": route_type,
            "route_rating": extract_route_rating(grade_raw.strip()),
            })

    crag['routes'] = routes
    return(crag)


def flatten_crag(crag):

    flat_crag = []
    for r in crag['routes']:
        flat_crag.append({
            'crag_id': crag['crag_id'],
            'route_grade': r['grade'],
            'route_type': r['route_type'],
            'route_rating': r['route_rating']
            })

    return(flat_crag)


def extract_all_crag_routes(crags):

    all_routes = []
    for crag in crags:
        crag_routes = flatten_crag(extract_crag_routes(crag))
        all_routes = all_routes + crag_routes

    return(all_routes)


def extract_route_type(raw_grade):

    if re.search('  [0-9][abc]', raw_grade):
        route_type = "sport"
        grade = ukc_class.sport_grade(raw_grade)
    elif re.search('[Vf][0-9]', raw_grade):
        route_type = "bouldering"
        grade = ukc_class.boulder_grade(raw_grade)
    elif re.search('([MDS]|E[0-9]+|none) ', raw_grade):
        route_type = "trad"
        grade = ukc_class.trad_grade(raw_grade)
    else:
        route_type = "unknown"
        grade = raw_grade.strip()
        print("WARNING: Unknown route type: {}".format(raw_grade))

    return(route_type, grade)


def extract_route_rating(raw_grade):
    # Finds a series or one or more '*' characters, returns [] otherwise
    rating = re.findall('[*]+', raw_grade)
    if rating == []:
        return(0)
    else:
        # We should only get one series of '*' characters from the regex
        star_string = rating[0]
        return(len(star_string))
