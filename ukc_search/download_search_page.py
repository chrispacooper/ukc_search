import random
import time
import requests
import os
from lxml import html
import requests_cache
from slugify import slugify

requests_cache.install_cache()


def download_search_pages(url, previous_pages=None):
    # Recursively download the results pages from a normal ukc search

    print("Downloading {}".format(url))

    r = requests.get(url)
    print("From cache: {}".format(r.from_cache))
    if not r.from_cache:
        # Pause between hitting website so we don't
        # overload it.
        print("Sleeping")
        time.sleep(random.uniform(1, 5))

    if previous_pages is None:
        search_pages = []
    else:
        search_pages = previous_pages

    search_pages.append({
        'url': url,
        'webpage': r.text
        })

    download_next_page(r.text, search_pages)

    return(search_pages)


def download_next_page(existing_page, previous_pages):
    # If there is another page of results, download it

    tree = html.fromstring(existing_page)

    x_selector = '//a[contains(@title, "Next page")]'
    x_selection = tree.xpath(x_selector)

    href_set = set()
    for element in x_selection:
        href_set.add(element.get("href"))

    if len(href_set) == 1:
        (url_end,) = href_set
        url = 'https://www.ukclimbing.com/logbook/map/' + url_end
        time.sleep(random.uniform(1, 5))
        download_search_pages(url, previous_pages)


def download_html(url, search_folder="data/search_page"):

    results = download_search_pages(url)
    print(len(results))
    for r in results:
        print("Saving {}".format(r['url']))
        file_name = slugify(r['url'])
        file_path = os.path.join(search_folder, file_name) + ".html"

        with open(file_path, "w") as fp:
            fp.write(r['webpage'])
