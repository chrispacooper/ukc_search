import pandas as pd
import ukc_search.grade_class as ukc_class

pd.set_option('display.max_colwidth', -1)


def filter_crags(routes, route_type_filter,
                 route_rating_filter, min_route_cnt,
                 min_grade=None, max_grade=None):

    print("Filtering for {} or more {} routes with at least {} stars"
          .format(min_route_cnt, route_type_filter, route_rating_filter)
          )

    # Create dataframe of routes (one row per route)
    routes = pd.DataFrame(routes)

    # Filter to just routes of correct type and sufficient quality
    routes_filtered = (
            routes
            .query(
                'route_type == @route_type_filter'
                )
            .query(
                'route_rating >= @route_rating_filter'
                )
            )

    if min_grade:
        min_grade = ukc_class.grade(min_grade, route_type_filter)
        routes_filtered = routes_filtered.query(
                'route_grade >= @min_grade')

    if max_grade:
        max_grade = ukc_class.grade(max_grade, route_type_filter)
        routes_filtered = routes_filtered.query(
                'route_grade <= @max_grade')

    # Aggregate dataframe to get count of routes (one row per crag)
    crags = (
            routes_filtered
            .groupby(['crag_id', 'route_type'])
            .size()
            .reset_index(name='cnt_routes')
            )

    # Filter to just crags with enough routes
    crags_filtered = crags.query('cnt_routes >= @min_route_cnt')

    url_root = 'https://www.ukclimbing.com/logbook/crag.php?id='
    df_res = (
             crags_filtered
             .assign(
                 crag_url=lambda x: url_root + x.crag_id.astype(str)
                 )
             )

    return(df_res)


def print_filtered_crags(df):
    print(df.to_string(index=False))
