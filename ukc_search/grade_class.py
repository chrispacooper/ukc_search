"""
Class definitions for tbe various ways to grade a climb.

This allows us to use the standard >= symbols elsewhere to
compare the difficulty of climbs.
"""

import re


class climb_grade():
    """
    Generic class used by classes for paticular route types.

    Provides error handling for grades that can't be interpreted.
    """

    def __init__(self, raw_grade):
        """
        Initialise and indicate if it was successful.

        If the grade was interpreted correctly then grade_valid
        will be true.
        """
        try:
            self.initialise(raw_grade)
            self.grade_valid = True
        except:
            self.grade_valid = False
            self.grade_string = raw_grade
            print("Could not interpret {} grade from '{}'"
                  .format(self.route_type, raw_grade))

    def _check_comparable(self, other):
        """Check whether it makes sense to compare grades."""

        grades_valid = self.grade_valid and other.grade_valid
        if not grades_valid:
            print("WARNING: can't compare grades '{}' and '{}'"
                  .format(self.grade_string, other.grade_string))

        same_type = type(self) == type(other)
        if not same_type:
            raise Exception(
                    "Cannot compare {} and {}"
                    .format(type(self), type(other)))

        return(grades_valid)

    def __ge__(self, other):
        """
        Greater than or equal.

        False if grades are not comparable, otherwise
        use method defined by classes below.
        """
        if self._check_comparable(other):
            ge = self._greater_equal(other)
        else:
            ge = False

        return(ge)

    def __le__(self, other):
        """
        Less than or equal.

        False if grades are not comparable, otherwise
        use method defined by classes below.
        """
        if self._check_comparable(other):
            le = self._less_equal(other)
        else:
            le = False

        return(le)


class sport_grade(climb_grade):
    """Sport Grade class."""

    route_type = "sport"

    def initialise(self, raw_grade):
        """
        Create a sport grade from a string.

        e.g. sport_grade('7a').
        """
        number = re.findall('[0-9]+', raw_grade)[0]
        letter = re.findall('[abc]', raw_grade)[0]
        self.number_grade = number
        self.letter_grade = letter
        self.grade_string = number + letter

    def _greater_equal(self, other):
        """
        Compare the difficulty with another sport grade.

        e.g. sport_grade('8a') >= sport_grade('6c')
        """
        if self.number_grade > other.number_grade:
            ge = True
        elif (
                (self.number_grade == other.number_grade)
                and
                (self.letter_grade >= other.letter_grade)
                ):
            ge = True
        else:
            ge = False

        return(ge)

    def _less_equal(self, other):
        """
        Compare the difficulty with another sport grade.

        e.g. sport_grade('6a') <= sport_grade('7c')
        """
        return(other >= self)


class boulder_grade(climb_grade):
    """
    Boulder Grade class.

    Accepts V or font grading and uses a conversion from
    rockfax to compare the difficulty across those systems.

    e.g. boulder_grade("V11") >= boulder_grade("f6B")
    """

    route_type = "bouldering"

    # For comparisons between grading systems, I am using the
    # integer part of the V grade, and referring to that as
    # the "internal grade".
    # Rockfax provide a conversion table which I have copied here.
    font_internal_conversion = {
            "1" : 0,
            "2" : 0,
            "3" : 0,
            "4" : 0,
            "5" : 1,
            "6" : 3,
            "6A": 3,
            "6B": 4,
            "6C": 5,
            "7" : 6,
            "7A": 6,
            "7B": 8,
            "7C": 9,
            "8" : 11,
            "8A": 11,
            "8B": 13,
            "8C": 15,
            }

    def initialise(self, raw_grade):
        """
        Create a boulder grade from a string.

        One key output is the internal grade used to compare the
        difficulty of problems.
        e.g.
        boulder_grade("V11")
        boulder_grade("f6A")
        """
        # Sometimes both types of grade are given, so take the first
        grade_matches = re.findall('[Vf][0-9]+[A-C]*', raw_grade)
        if 1 <= len(grade_matches) <= 2:
            grade_match = grade_matches[0]

        # Grade system indictor is V or f
        grade_system_indicator = re.findall('([Vf])[0-9]', grade_match)

        if grade_system_indicator[0] == "V":
            grade_info = self._extract_grade_v(grade_match)
        elif grade_system_indicator[0] == "f":
            grade_info = self._extract_grade_font(grade_match)

        # V or font
        self.grade_system = grade_info['grade_system']
        # Integer part of V grade
        self.internal_grade = grade_info['internal_grade']
        # Human readable representation of grade
        self.grade_string = grade_info['grade_string']

    def _extract_grade_v(self, raw_grade):

        grade_system = "V"

        v_grade_number = re.findall('V([0-9]+)', raw_grade)
        assert(len(v_grade_number) == 1), (
                "Couldn't extract V grade for: {}"
                .format(raw_grade))
        v_grade_number = v_grade_number[0]

        internal_grade = int(v_grade_number)
        grade_string = grade_system + v_grade_number

        grade_info = {
                'grade_system': grade_system,
                'internal_grade': internal_grade,
                'grade_string': grade_string,
                }
        return(grade_info)

    def _extract_grade_font(self, raw_grade):

        grade_system = "font"

        font_grade = re.findall('f([0-9]+[A-C]*)', raw_grade)
        assert(len(font_grade) == 1), (
                "Couldn't extract font grade for: {}"
                .format(raw_grade))
        font_grade = font_grade[0]

        internal_grade = self.font_internal_conversion[font_grade]
        grade_string = grade_system + font_grade

        grade_info = {
                'grade_system': grade_system,
                'internal_grade': internal_grade,
                'grade_string': grade_string,
                }
        return(grade_info)

    def _greater_equal(self, other):
        """Compare problem difficulty."""
        return(self.internal_grade >= other.internal_grade)

    def _less_equal(self, other):
        """Compare problem difficulty."""
        return(self.internal_grade <= other.internal_grade)

    def __repr__(self):
        """Display human readable string representing grade."""
        return(self.grade_string)


class trad_grade(climb_grade):
    """
    Trad Grade class.

    Uses just the experiental grade to compare the difficulty.

    e.g. trad_grade("HVS 6c") <= trad_grade("E1 6a")
    """

    route_type = "trad"

    # Order of difficulty for trad grades, taken from rockfax
    experience_ordering = (
            "none",
            "MS",  # no idea what MS means, should google
            "MVS",  # not sure where this should fit either
            "M",
            "D",
            "HD",  # not sure where this should fit either
            "VD",
            "HVD",
            "S",
            "HS",
            "VS",
            "HVS",
            "E",
            )

    experience_internal_conversion = {}
    i = 0
    # Create a lookup from experience grade to an internal integer grade
    for g in experience_ordering:
        experience_internal_conversion[g] = i
        i = i + 1

    def initialise(self, raw_grade):
        """
        Create a trad grade from a string.

        One key output is the internal grade used to compare the
        difficulty of routes. This only considers the experience
        grade.
        e.g. trad_grade("HVS 6a")
        """
        experience_letter = re.findall('([HVMDS]+|E|none)', raw_grade)[0]
        experience_number = re.findall('E([0-9]+)', raw_grade)

        move_grade = re.findall('[0-9][a-c]', raw_grade)
        if len(move_grade) == 0:
            move_grade = ''
        elif len(move_grade) <= 2:
            # Just take the first grade if a / is given
            # i.e. keep 6c if 6c/7a is given
            move_grade = move_grade[0]

        # Convert experience grade to internal integer grade
        if experience_letter == "E":
            # For E grades add the number after the E to the internal grade
            # taken from the lookup
            internal_grade = (
                    self.experience_internal_conversion[experience_letter]
                    + int(experience_number[0]))
            experience_grade = experience_letter + experience_number[0]
        else:
            # Just use lookup
            internal_grade = (
                    self.experience_internal_conversion[experience_letter])
            experience_grade = experience_letter

        self.internal_grade = internal_grade
        self.grade_string = experience_grade + " " + move_grade

    def _greater_equal(self, other):
        """Compare route difficulty."""
        return(self.internal_grade >= other.internal_grade)

    def _less_equal(self, other):
        """Compare route difficulty."""
        return(self.internal_grade <= other.internal_grade)

    def __repr__(self):
        """Display human readable string representing grade."""
        return(self.grade_string)


def grade(grade_raw, route_type):
    """Return a grade object of the appropriate class."""
    if route_type == "sport":
        g = sport_grade(grade_raw)
    elif route_type == "bouldering":
        g = boulder_grade(grade_raw)
    elif route_type == "trad":
        g = trad_grade(grade_raw)
    else:
        raise Exception(
                "Provided route type does not exist: {}"
                .format(route_type))

    return(g)
